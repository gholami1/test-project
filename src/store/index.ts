// Dependence
import { configureStore } from '@reduxjs/toolkit';

// Store
import InputReducer from 'store/reducers/global';

export const store = configureStore({
  reducer: {
    InputReducer
  },
  devTools: true
});

export type AppStore = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
