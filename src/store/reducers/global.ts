// Dependence
import { createSlice } from '@reduxjs/toolkit';

export interface InputState {
  inputState: string;
}

// Initial state
const initialState: InputState = {
  inputState: ''
};

// Actual Slice
export const inputReducer = createSlice({
  name: 'inputText',
  initialState,
  reducers: {
    // Action to set the data input status
    changeInputState(state, action) {
      state.inputState = action?.payload;
    }
  }
});

export const { changeInputState } = inputReducer.actions;

export default inputReducer.reducer;
