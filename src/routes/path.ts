// Dependence
import React from 'react';

const Products = React.lazy(async () => await import('view/products'));
const Users = React.lazy(async () => await import('view/users'));
const Verfication = React.lazy(async () => await import('view/verification'));

interface RouteMapProps {
  path: string;
  titleName: string;
  component: React.LazyExoticComponent<React.ComponentType<any>>;
  auth: boolean;
}

const routeMap: RouteMapProps[] = [
  {
    path: '/products',
    titleName: 'محصولات',
    component: Products,
    auth: false
  },
  {
    path: '/users',
    titleName: 'کاربران',
    component: Users,
    auth: false
  },
  {
    path: '/verfication',
    titleName: 'اعتبارسنجی',
    component: Verfication,
    auth: true
  }
];

export { routeMap };
