// Dependence
import { useEffect } from 'react';
import { Route, Routes, useLocation, useNavigate } from 'react-router-dom';

// Routes
import { routeMap } from './path';

const PrivateRoute = () => {
  const locations = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (locations.pathname === '/') {
      navigate('/verfication');
    }
  }, []);

  return (
    <Routes>
      {routeMap?.map((route, index) => {
        if (route.auth) {
          return <Route key={index} path={route.path} element={<route.component />} />;
        }

        return <Route key={index} path={route.path} element={<route.component />} />;
      })}
    </Routes>
  );
};

export default PrivateRoute;
