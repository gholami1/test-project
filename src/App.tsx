// Dependence
import { Suspense } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

// Routes
import PrivateRoute from './routes/PrivateRoute';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Suspense fallback={<div>Loading...</div>}>
          <Routes>
            <Route path="*" element={<PrivateRoute />} />
          </Routes>
        </Suspense>
      </BrowserRouter>
    </div>
  );
}

export default App;
