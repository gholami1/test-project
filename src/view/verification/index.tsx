// Dependence
import { useState, useEffect } from 'react';

// Components
import AuthInput from 'components/authInput';

// Styles
import styles from './styles.module.scss';

// View
import InputModal from 'view/verification/input_modal';

const Verification = () => {
  const [otp, setOtp] = useState('');
  const onChange = (value: string) => {
    setOtp(value);
  };
  const [showModal, setShowModal] = useState(false);

  const keydownHandler = (e: any) => {
    if (e.ctrlKey && e.shiftKey && e.key === 'F2') {
      setShowModal(true);
    }
  };

  useEffect(() => {
    window.addEventListener('keydown', keydownHandler);
    return () => {
      window.removeEventListener('keydown', keydownHandler);
    };
  }, []);

  const handleClear = () => {
    setOtp('');
  };

  const handleVerify = () => {};

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <div className={styles.verification_code}>
        <div>
          <AuthInput value={otp} valueLength={4} onChange={onChange} />
        </div>
        <div>
          <button className={[styles.btn_box, styles.btn_verify].join(' ')} onClick={handleVerify}>
            VERIFY CODE
          </button>
          <button className={[styles.btn_box, styles.btn_clear].join(' ')} onClick={handleClear}>
            CLEAR
          </button>
        </div>
        <div>
          <button className={styles.correct_phone_number_btn}>correct phone number</button>
        </div>
      </div>
      {showModal && (
        <InputModal
          title={'title'}
          onClose={() => {
            handleCloseModal();
          }}
        />
      )}
    </>
  );
};

export default Verification;
