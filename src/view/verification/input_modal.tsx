// Dependence
import { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Store
import { changeInputState } from 'store/reducers/global';
import type { AppStore } from 'store';

// Styles
import styles from './styles.module.scss';

interface InputModalProps {
  title?: string;
  onClose: () => void;
}
const InputModal = ({ onClose, title }: InputModalProps) => {
  const refInput = useRef<HTMLInputElement>(null);

  const [val, setVal] = useState('');

  const dispatch = useDispatch();

  const textShow = useSelector((state: AppStore) => state.InputReducer.inputState);

  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    setVal(target.value);
    dispatch({ type: changeInputState('').type, payload: target.value });
  };

  const keydownHandler = (e: KeyboardEvent) => {
    if (e.ctrlKey && e.shiftKey && e.key === 'F3' && refInput) {
      refInput?.current?.focus();
    }
  };

  useEffect(() => {
    window.addEventListener('keydown', keydownHandler);
    return () => {
      window.removeEventListener('keydown', keydownHandler);
    };
  }, []);

  return (
    <div className={styles.modal_box}>
      <div className={styles.modal} onClick={onClose}>
        <div
          className={styles.modal_content}
          onClick={(e: React.MouseEvent<HTMLDivElement>) => {
            e.stopPropagation();
          }}>
          <div className={styles.modal_header}>
            <h4 className={styles.modal_title}>{title}</h4>
          </div>
          <div className={styles.modal_body}>
            <div className={styles.box}>
              <input
                name="name"
                value={val}
                onChange={handleChange}
                ref={refInput}
                className={styles.input_content}
              />
            </div>
            <div className={styles.box}>
              <p>{textShow}</p>
            </div>
          </div>
          <div className={[styles.modal_footer, styles.footer_box].join(' ')}>
            <button onClick={onClose} className={styles.button}>
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InputModal;
